<?php

namespace App\Infrastructure\Repository\Doctrine;

use App\Domain\Model\Participant\Participant;
use Doctrine\Persistence\ManagerRegistry;
use App\Domain\Port\Output\ParticipantStorageInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use App\Domain\Model\Participant\Exception\ParticipantNotFoundException;
use App\Domain\Model\Tournament\Tournament;

/**
 * @method Participant|null find($id, $lockMode = null, $lockVersion = null)
 * @method Participant|null findOneBy(array $criteria, array $orderBy = null)
 * @method Participant[]    findAll()
 * @method Participant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ParticipantRepository extends ServiceEntityRepository implements ParticipantStorageInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Participant::class);
    }

    public function getById(string $participantId): ?Participant
    {
        $participant = $this->find($participantId);
        if (!$participant) throw new ParticipantNotFoundException();
        
        return $participant;
    }

    public function create(Participant $participant): Participant 
    {
        $this->_em->persist($participant);
        $this->_em->flush();

        return $participant;
    }

    public function addParticipantToTournament(Participant $participant, Tournament $tournament): Participant
    {
        $tournament->addParticipant($participant);

        $this->_em->flush();
        
        return $participant;
    }

}