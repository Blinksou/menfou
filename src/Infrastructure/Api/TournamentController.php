<?php

namespace App\Infrastructure\Api;

use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Domain\Model\Tournament\TournamentBuilder;
use App\Domain\Port\Input\Queries\GetTournamentQuery;
use App\Domain\Port\Input\Commands\CreateTournamentCommand;
use App\Domain\Model\Tournament\Validator\TournamentValidator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class TournamentController extends AbstractController
{
    public function __construct(
        private CreateTournamentCommand $createTournamentCommand,
        private TournamentValidator $tournamentValidator,
        private TournamentBuilder $tournamentBuilder,
        private GetTournamentQuery $getTournamentQuery
    ) {
    }

    #[Route("/api/tournaments", name: "tournament.create", methods: [Request::METHOD_POST])]
    public function createTournament(Request $request): Response
    {
        try {
            $content = json_decode($request->getContent(), true);
            $this->tournamentValidator->validate($content);
            $tournament = $this->createTournamentCommand->execute($this->tournamentBuilder->partialTournamentToCreate($content));
        } catch (Exception $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        
        return $this->json(['id' => $tournament->getId()], Response::HTTP_CREATED);
    }

    #[Route("/api/tournaments/{id}", name: "tournament.get", methods: [Request::METHOD_GET])]
    public function getTournament(string $id): Response
    {
        try {
            $tournament = $this->getTournamentQuery->execute($id);
        } catch (Exception $e) {
            throw new NotFoundHttpException($e->getMessage());
        }
        
        return $this->json(['tournament' => $tournament], Response::HTTP_CREATED);
    }
}