<?php

namespace App\Infrastructure\Api;

use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Domain\Model\Participant\ParticipantBuilder;
use App\Domain\Port\Input\Commands\CreateParticipantCommand;
use App\Domain\Model\Participant\Validator\ParticipantValidator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ParticipantController extends AbstractController
{
    public function __construct(
        private CreateParticipantCommand $createParticipantCommand,
        private ParticipantValidator $participantValidator,
        private ParticipantBuilder $participantBuilder,
    ) {
    }

    #[Route(" /api/tournaments/{tournamentId}/participants", name: "participant.create", methods: [Request::METHOD_POST])]
    public function createParticipantOnTournament(string $tournamentId, Request $request): Response
    {
        try {
            $content = json_decode($request->getContent(), true);
            $this->participantValidator->validate($content);
            $participant = $this->createParticipantCommand->execute($this->participantBuilder->partialParticipantToCreate($content), $tournamentId);
        } catch (Exception $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        return $this->json(['id' => $participant->getId()], Response::HTTP_CREATED);
    }
}