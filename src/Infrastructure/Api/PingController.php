<?php

namespace App\Infrastructure\Api;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route("/api/ping", name: "app.ping")]
class PingController extends AbstractController
{
    // https://gitlab.com/Blinksou/menfou
    public function __invoke(): Response
    {
        return $this->json(['status' => 'OK']);
    }
}