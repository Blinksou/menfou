<?php

namespace App\Domain\Port\Output;

use App\Domain\Model\Participant\Participant;
use App\Domain\Model\Tournament\Tournament;

interface ParticipantStorageInterface
{
    public function getById(string $participantId): ?Participant;
    public function create(Participant $participant): Participant;
    public function addParticipantToTournament(Participant $participant, Tournament $tournament): Participant;
}