<?php

namespace App\Domain\Port\Input\Queries;

use App\Domain\Model\Tournament\Tournament;
use App\Domain\Port\Output\TournamentStorageInterface;

class GetTournamentQuery
{
    public function __construct(
        private TournamentStorageInterface $tournamentStorage
    ) {
    }

    public function execute(string $tournamentId): ?Tournament
    {
        return $this->tournamentStorage->getById($tournamentId);
    }
}