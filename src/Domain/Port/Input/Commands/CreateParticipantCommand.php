<?php

namespace App\Domain\Port\Input\Commands;

use App\Domain\Model\Participant\Participant;
use App\Domain\Port\Input\Queries\GetTournamentQuery;
use App\Domain\Port\Output\ParticipantStorageInterface;

class CreateParticipantCommand
{
    public function __construct(
        private ParticipantStorageInterface $participantStorage,
        private GetTournamentQuery $getTournamentQuery
    ) {
    }

    public function execute(Participant $participantOnCreation, string $tournamentId): Participant
    {
        $participant = $this->participantStorage->create($participantOnCreation);
        $tournament = $this->getTournamentQuery->execute($tournamentId);
    
        $this->participantStorage->addParticipantToTournament($participant, $tournament);
        
        return $participant;
    }
}