<?php

namespace App\Domain\Model\Participant;

use App\Domain\Model\Participant\Participant;

class ParticipantBuilder
{
    public function partialParticipantToCreate(array $parameters): Participant
    {
        $participant = new Participant();
        foreach ($parameters as $key => $param) {
            $method = 'set' . ucfirst($key);

            if (method_exists($participant, $method)) {
                $participant->$method($param);
            }
        }
        return $participant;
    }
}