<?php

namespace App\Domain\Model\Participant\Validator;

use App\Domain\Model\Participant\Exception\ParticipantNotValidException;
use App\Domain\Model\Tournament\Tournament;

class ParticipantValidator
{
    public function validate(array $participantData):void
    {
        foreach ($participantData as $key => $data) {
            switch ($key) {
                case 'name':
                    if (!is_string($data) || empty($data)) throw new ParticipantNotValidException();
                    break;
                case 'elo':
                    if (!is_int($data) || empty($data)) throw new ParticipantNotValidException();
                    break;
                default:
                    throw new ParticipantNotValidException();
            }
        }
    }
}
