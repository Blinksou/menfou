<?php

namespace App\Domain\Model\Participant\Exception;

use Exception;

class ParticipantNotValidException extends Exception
{
    protected $message = "Les données du participant fournies ne sont pas valides";
}