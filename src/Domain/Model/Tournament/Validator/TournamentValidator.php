<?php

namespace App\Domain\Model\Tournament\Validator;

use App\Domain\Model\Tournament\Exception\TournamentNotValidException;
use App\Domain\Model\Tournament\Tournament;

class TournamentValidator
{
    public function validate(array $tournamentData):void
    {
        foreach ($tournamentData as $key => $data) {
            switch ($key) {
                case 'name':
                    if (!is_string($data)) throw new TournamentNotValidException();
                    break;
                case 'type':
                    if (!in_array($data, Tournament::TYPES)) throw new TournamentNotValidException();
                    break;
                default:
                    throw new TournamentNotValidException();
            }
        }
    }
}
