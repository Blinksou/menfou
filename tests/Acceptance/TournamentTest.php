<?php

namespace App\Tests\Acceptance;

use App\Tests\WebTestCaseWithDatabase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TournamentTest extends WebTestCaseWithDatabase
{
    public const TOURNAMENT_NAME = 'Rolland Garros';
    public const JSON_HEADERS = ['Content-Type: application/json', 'Accept: application/json'];
     
    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     * @throws \JsonException
     */
    public function testTournamentCreation(): void
    {
        $client = static::createClient();
        
        $client->request(Request::METHOD_POST,'/api/tournaments',
            [
                'headers' => self::JSON_HEADERS,
                'body' => json_encode(['name' => self::TOURNAMENT_NAME], JSON_THROW_ON_ERROR)
            ]
        );

        self::assertResponseStatusCodeSame(Response::HTTP_CREATED);
    }

    public function testTournamentCreationShouldEnableToRetrieveAfter(): void
    {
        // test here
        $client = static::createClient();
        
        $client->request(Request::METHOD_POST,'/api/tournaments',
            [
                'headers' => self::JSON_HEADERS,
                'body' => json_encode(['name' => self::TOURNAMENT_NAME], JSON_THROW_ON_ERROR)
            ]
        );

        $response = $client->getResponse()->toArray();

        $client->request(Request::METHOD_GET,"/api/tournaments/{$response['id']}",
            [
                'headers' => self::JSON_HEADERS,
            ]
        );

        self::assertResponseIsSuccessful();
        self::assertJsonContains(['tournament' => ['name'=>self::TOURNAMENT_NAME]]);
    }
}