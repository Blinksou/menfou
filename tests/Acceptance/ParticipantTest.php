<?php

namespace App\Tests\Acceptance;

use App\Tests\WebTestCaseWithDatabase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ParticipantTest extends WebTestCaseWithDatabase
{
    public const PARTICIPANT_NAME = 'Rafael Nadal';
    public const TOURNAMENT = 'Tournoi Hexagonal';
    public const JSON_HEADERS = ['Content-Type: application/json', 'Accept: application/json'];
     
    public function setUp(): void
    {
        parent::setUp();
    }

    public function createTournament($name): int
    {
        $this->client->request(Request::METHOD_POST, '/api/tournaments', [
            'headers' => [
                'Content-Type: application/json',
                'Accept: application/json',
            ],
            'body' => json_encode(['name' => $name])
        ]);

        return $this->client->getResponse()->toArray()['id'];
    }

    /**
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     * @throws \JsonException
     */
    public function testParticipantCreation(): void
    {
        $tournamentId = $this->createTournament(self::TOURNAMENT);      

        $this->client->request(Request::METHOD_POST,"/api/tournaments/$tournamentId/participants",
            [
                'headers' => self::JSON_HEADERS,
                'body' => json_encode(['name' => self::PARTICIPANT_NAME], JSON_THROW_ON_ERROR)
            ]
        );

        self::assertResponseIsSuccessful();
        $response = $this->client->getResponse()->toArray();
        self::assertIsInt($response["id"]);
    }
}